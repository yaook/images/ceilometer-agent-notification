##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
FROM python:3.10-slim-bookworm

ARG release
ARG ceilometer_uid=2500002
ARG ceilometer_gid=2500002

USER root

RUN set -eux; \
    groupadd --gid ${ceilometer_gid} ceilometer; \
    adduser --disabled-password --gecos "" --uid ${ceilometer_uid} --gid ${ceilometer_gid} ceilometer

RUN set -eux; \
    export LANG=C.UTF-8; \
    export DEBIAN_FRONTEND=noninteractive; \
    export DEV_PACKAGES="gcc git python3-dev"; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        $DEV_PACKAGES \
        netbase \
        python3-pip \
        python3-setuptools; \
    (git clone https://github.com/openstack/requirements.git --depth 1 --branch stable/${release} || \
    git clone https://github.com/openstack/requirements.git --depth 1 --branch ${release}-eol || \
    git clone https://github.com/openstack/requirements.git --depth 1 --branch unmaintained/${release}); \
    sed -i '/^ceilometer===.*/d' /requirements/upper-constraints.txt; \
    (git clone https://github.com/openstack/ceilometer.git --depth 1 --branch stable/${release} || \
    git clone https://github.com/openstack/ceilometer.git --depth 1 --branch ${release}-eol || \
    git clone https://github.com/openstack/ceilometer.git --depth 1 --branch unmaintained/${release}); \
    pip3 install wheel; \
    pip3 install -c requirements/upper-constraints.txt PyMySQL python-memcached pymemcache gnocchiclient panko ceilometer/; \
    apt-get purge -y $DEV_PACKAGES; \
    apt-get clean autoclean; \
    apt-get autoremove --yes; \
    rm -rf /var/lib/{apt,dpkg,cache,log}/; \
    rm -rf requirements ceilometer

USER ceilometer

CMD ceilometer-agent-notification
